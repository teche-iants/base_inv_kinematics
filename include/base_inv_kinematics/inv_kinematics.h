#pragma once

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <math.h>
#include <Eigen/Dense>

using namespace std;
using Eigen::MatrixXd;
namespace inv_kinematics{

    class Inv_Kinematics
    {
        
        private:
            ros::Publisher fl_wheel_cmd_vel;
            ros::Publisher fr_wheel_cmd_vel;
            ros::Publisher bl_wheel_cmd_vel;
            ros::Publisher br_wheel_cmd_vel;

            ros::Subscriber cmd_vel_subscriber;             
            ros::NodeHandle n;
            int NODE_RATE = 50;
            const float GEAR_RATIO = 24/18;
            const float RAD_S_TO_M_S = 19.685039;
            const float WHEEL_RADIUS                 = 0.0508; //meters
            const float DISTANCE_LEFT_TO_RIGHT_WHEEL = 0.3556;
            const float DISTANCE_FRONT_TO_REAR_WHEEL = 0.381;

            const float WHEEL_SEPARATION_WIDTH = DISTANCE_LEFT_TO_RIGHT_WHEEL / 2;
            const float WHEEL_SEPARATION_LENGTH = DISTANCE_FRONT_TO_REAR_WHEEL / 2;
            const float C1 = 1/WHEEL_RADIUS;

            float x_speed = 0; 
            float y_speed = 0; 
            float yaw     = 0;

            float wheel_front_left  = 0;
            float wheel_front_right = 0;
            float wheel_rear_left   = 0;
            float wheel_rear_right  = 0;

            std_msgs::Float64 W1, W2, W3, W4;
        public:
            Inv_Kinematics()
            {
                fl_wheel_cmd_vel = n.advertise<std_msgs::Float64>("/base/fl_wheel/cmd_vel", 1);
                fr_wheel_cmd_vel = n.advertise<std_msgs::Float64>("/base/fr_wheel/cmd_vel", 1);
                bl_wheel_cmd_vel = n.advertise<std_msgs::Float64>("/base/bl_wheel/cmd_vel", 1);
                br_wheel_cmd_vel = n.advertise<std_msgs::Float64>("/base/br_wheel/cmd_vel", 1);

                cmd_vel_subscriber = n.subscribe("/cmd_vel", 10, &Inv_Kinematics::cmd_vel_callback, this);
            }
        //Functions();
            void init_variables();
            void spin();
            void mecanum_inv_kinematics(float Vx, float Vy, float Wo, float *fl_W1, float *fr_W2, float *bl_W3, float *br_W4);
            void cmd_vel_callback(const geometry_msgs::Twist& msg);
            void pub_wheel_vel();
    };
};