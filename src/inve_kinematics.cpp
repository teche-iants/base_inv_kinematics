#include<base_inv_kinematics/inv_kinematics.h>

namespace inv_kinematics{
    void Inv_Kinematics::init_variables(){
        
    }

    void Inv_Kinematics::spin(){
        ros::Rate loop_rate(NODE_RATE);
        ROS_INFO("******Entering Loop************");
        while(ros::ok()){ 
            pub_wheel_vel();
            ROS_INFO("-------------");
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void Inv_Kinematics::cmd_vel_callback(const geometry_msgs::Twist& msg){
        x_speed = msg.linear.x*GEAR_RATIO;
        y_speed = msg.linear.y*GEAR_RATIO;
        yaw = (msg.angular.z/RAD_S_TO_M_S)*GEAR_RATIO;
        //ROS_INFO("x_speed = %f", x_speed);
    }

    void Inv_Kinematics::mecanum_inv_kinematics(float Vx, float Vy, float Wo, float *fl_W1, float *fr_W2, float *bl_W3, float *br_W4){
        MatrixXd Wv(4,1);
        MatrixXd Vo(3,1); //generalized velocity of the center point of the robot
        
        Vo << Vx,
              Vy,
              Wo;

        MatrixXd J(4,3);
        J << 1,  1,  C1,
             1, -1, -C1,
             1,  1, -C1,
             1, -1,  C1;

        Wv = (1/WHEEL_RADIUS) * J * Vo;

        *fl_W1 = Wv(1,0);
        *fr_W2 = Wv(0,0);
        *bl_W3 = Wv(2,0);
        *br_W4 = Wv(3,0);
    }

    void Inv_Kinematics::pub_wheel_vel(){
        float fl_W1, fr_W2, bl_W3, br_W4;
        mecanum_inv_kinematics(x_speed, y_speed, yaw, &fl_W1, &fr_W2, &bl_W3, &br_W4);

        W1.data = fl_W1;
        W2.data = fr_W2;
        W3.data = bl_W3;
        W4.data = br_W4;

        fl_wheel_cmd_vel.publish(W1);
        fr_wheel_cmd_vel.publish(W2);
        bl_wheel_cmd_vel.publish(W3);
        br_wheel_cmd_vel.publish(W4);
    }
};