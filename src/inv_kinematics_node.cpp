#include<base_inv_kinematics/inv_kinematics.h>

using namespace inv_kinematics;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "inv_kinematics");
	Inv_Kinematics m_;
	m_.spin();
	return 0;
}